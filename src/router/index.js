import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// route-level code splitting

import index_view from '../views/index.vue'
import platform_service from '../views/platform_service.vue'
import logistics from '../views/logistics.vue'
import medical_solution from '../views/medical_solution.vue'
import source_solution from '../views/source_solution.vue'
import provements_solution from '../views/provements_solution.vue'
import about_us from '../views/about_us.vue'
import assets_record from '../views/assets_record.vue'
import News from '../views/news.vue'
import news_item from '../views/news_item.vue'

export function createRouter () {
  return new Router({
    mode: 'history',
    fallback: false,
    scrollBehavior: () => ({ y: 0 }),
    routes: [
      { path: '/platform_service/:page(\\d+)?', component: platform_service, name: 'platform_service' },
      { path: '/logistics/:page(\\d+)?', name: 'logistics', component: logistics },
      { path: '/medical_solution/:page(\\d+)?', component: medical_solution, name: 'medical_solution' },
      { path: '/source_solution/:page(\\d+)?', component: source_solution, name: 'source_solution' },
      { path: '/provements_solution/:page(\\d+)?', component: provements_solution, name: 'provements_solution' },
      { path: '/about_us/:page(\\d+)?', component: about_us, name: 'about_us' },
      { path: '/assets_record/:page(\\d+)?', component: assets_record, name: 'assets_record' },
      { path: '/news/:page(\\d+)?', component: News, name: 'news' },
      { path: '/news_item/:news_id(\\d+)?', component: news_item, name: 'news_item' },
      { path: '/index/:page(\\d+)?', name: 'app', component: index_view },
      { path: '/', redirect: '/index' }
    ]
  })
}



